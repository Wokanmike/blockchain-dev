# Blockchain Dev

This is a repo where you can find tests and experiments releated with blockchain, using tecnologies like solidity, open zepellin and truffle, also could be used like a guide or simple reference of the lenguage used.

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/Wokanmike/blockchain-dev.git
git branch -M main
git push -uf origin main
```
## Project structure

```
project
│   README.md
│   file001.txt    
│
└───merkleRoot
│   merkleRoot.js
│   package.json
│   package-lock.json
│   
└───Solidity
│   crowfunding.sol

```