//noprotect
const fetch = require('node-fetch');
const sha256 = require('js-sha256');

const fetchLatestBlock = () =>
  fetch('https://blockchain.info/q/latesthash?cors=true')
  .then(r => r.text());

const fetchMerkelRootAndTransactions = block =>
  fetch(`https://blockchain.info/rawblock/${block}?cors=true`)
  .then(r => r.json())
  .then(d => [d.mrkl_root, d.tx.map(t => t.hash)]);

const toBytes = hex =>
  hex.match(/../g).reduce((acc, hex) => [...acc, parseInt(hex, 16)], []);

const toHex = bytes =>
  bytes.reduce((acc, bytes) => acc + bytes.toString(16).padStart(2, '0'), '');

const toPairs = (arr) => 
 Array.from(Array(Math.ceil(arr.length / 2)), (_, i) => arr.slice(i * 2, i * 2 + 2));


const hashPair = (a , b = a) => {
    const bytes = toBytes(`${b}${a}`).reverse();
    const hashed = sha256.array(sha256.array(bytes));
    return toHex(hashed.reverse());
}

const merkleRoot = txs =>
  txs.length === 1
  ? txs[0]
  :merkleRoot(toPairs(txs).reduce((tree, pair) => [...tree, hashPair(...pair)], []));

//Creates the merkleRoot from all the transactions obtained from the source, and generate the root hashing the pairs
// fetchLatestBlock()
// .then(fetchMerkelRootAndTransactions)
// .then(([root, txs]) => {
//   const isValid = merkleRoot(txs) === root;
//   console.log(isValid);
// })

const merkleProof = (txs, tx, proof = []) => {
  if (txs.length === 1) {
    return proof;
  }

  const tree = [];
  toPairs(txs).forEach(pair => {
    const hash = hashPair(...pair);

    if(pair.includes(tx)) {
      const idx = pair[0] === tx | 0;
      console.log(idx);
      proof.push([idx, pair[idx]]);
      tx = hash;
    }
    tree.push(hash);
  });
  return merkleProof(tree, tx, proof);
};

const random = arr =>
  arr[Math.floor(Math.random() * arr.length)];

const merkleproofRoot = (proof, tx) =>
  proof.reduce((root, [idx, tx]) => idx ? hashPair(root, tx) : hashPair(tx, root), tx);

fetchLatestBlock()
  .then(fetchMerkelRootAndTransactions)
  .then(([root, txs]) => {
    const tx = random(txs);
    const proof = merkleProof(txs, tx);
    
    const isValid = merkleproofRoot(proof, tx) === root;

    console.log(isValid);
})